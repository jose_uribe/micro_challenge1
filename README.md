# micro_challenge1

<br>
<br>

<div align="left"> 

### Project Team

#### [Jose Antonio Uribe](https://jose_uribe.gitlab.io/mdef/)
#### [Krzysztof Wronski](https://krzysztof_wronski.gitlab.io/wrona/)

<br>
<br>

### Project Description:

<div align="center"> 

##### - Create a tool that would help stimulate thought and dialogue, people’s perceived agency relating to different political issues. 
##### - Promote reflection and to collect a subjective assessment of their perceived agency on that particular topic.
##### - As we increasingly question the systems that shape and define our society, it is important to consider the agency level we feel we have in relation to changing or affecting those systems.
##### - An artifact to explore the intersection of humanism in design

<br>

<div align="left"> 

### Mechanics:

<div align="center"> 

##### - A measurment tool to quantify 
##### - Easy to use and intuitive
##### - Leverage Fab Academy resources and documentation
##### - Something that snaps and holds position

<div align="left"> 

### Tools:

<div align="center"> 

##### - Laser Cutter: MDF & Acrylic 
##### - Vynil Cutter: Vynil Masks
##### - Rhino: Parametric and 3d Design
##### - Velcro: Tittle Holders

--- 
<div align="left"> 

### Sketching

<div align="center"> 

<br>

![](images/img7.png) 

![](images/img5.png) 

<br>

![](images/img6.png)  


![](images/img9.PNG)

<br>

<div align="left"> 

### Prototyping

<div align="center"> 

![](images/img8.png)

![](images/88.png)

![](images/99.png)

<br>


<div align="left"> 

### Asigning Meaning

<div align="center"> 

![](images/img11.png)

<div align="left"> 

### Generating Dialogues

<div align="center"> 

![](images/1.png)

<br>

![](images/2.png)

<br>

![](images/3.png)

<br>

![](images/4.png)

<br>
<br>
<br>

# THE END

